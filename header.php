<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Pet Care Clinic
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="user-scalable=no"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
    <link rel="profile" href="<?php echo esc_url( __( 'http://gmpg.org/xfn/11', 'pet-care-clinic' ) ); ?>">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    
    <div id="preloader">
        <div class="loading-circle fa-spin"></div>
    </div>
    <div class="header">         
        <nav id="pet-care-clinic-top-nav" class="navbar" role="navigation">
            <div class="container">
                <div id="top-header">
                    <div class="row">
                        <div class="col-md-4 col-sm-4">
                            <div id="logo">
                                <div class="site-logo">
                                    <?php if( has_custom_logo() ){ the_custom_logo();
                                        }else { ?>
                                        <h1><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php
                                        bloginfo( 'name' ); ?></a></h1>
                                        <?php $description = get_bloginfo( 'description', 'display' );
                                        if ( $description || is_customize_preview() ) : ?>
                                        <p class="site-description"><?php echo esc_html($description); ?></p>
                                    <?php endif; } ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-8">
                            <div class="row">
                                <div class="col-md-4 col-sm-4">
                                    <div class="col-md-2">
                                        <i class="fas fa-map-marker-alt"></i>
                                    </div>
                                    <div class="col-md-10">
                                        <p><b><?php echo esc_html(get_theme_mod('pet_care_clinic_location_txt',''));?></b></p>
                                        <p><?php echo esc_html(get_theme_mod('pet_care_clinic_location',''));?></p>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <div class="col-md-2">
                                        <i class="far fa-clock"></i>
                                    </div>
                                    <div class="col-md-10">
                                        <p><b><?php echo esc_html(get_theme_mod('pet_care_clinic_time_txt',''));?></b></p>
                                        <p><?php echo esc_html(get_theme_mod('pet_care_clinic_time',''));?></p>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <div class="col-md-2">
                                        <i class="fas fa-mobile-alt"></i>
                                    </div>
                                    <div class="col-md-10">
                                        <p><b><?php echo esc_html(get_theme_mod('pet_care_clinic_call_txt',''));?></b></p>
                                        <p><?php echo esc_html(get_theme_mod('pet_care_clinic_call',''));?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header-menu pet-care-clinic-navbar">
                <div class="container">
                    <div class="col-md-9 col-sm-8">
                        <?php pet_care_clinic_header_menu(); ?>
                    </div>
                    <div class="col-md-3 col-sm-4">
                        <div class="social_icon">
                            <?php if(get_theme_mod("pet_care_clinic_social_fb")) : ?>
                                <span class="fbs">
                                    <a href="<?php echo esc_url(get_theme_mod('pet_care_clinic_social_fb',''));?>"><i class="fab fa-facebook-f"></i></a>
                                </span>
                            <?php endif; ?>
                            <?php if(get_theme_mod("pet_care_clinic_social_twitter")) : ?>
                                <span class="tws">
                                    <a href="<?php echo esc_url(get_theme_mod('pet_care_clinic_social_twitter',''));?>"><i class="fab fa-twitter"></i></a>
                                </span>
                            <?php endif; ?>
                            <?php if(get_theme_mod("pet_care_clinic_social_gp")) : ?>
                                <span class="gps">
                                    <a href="<?php echo esc_url(get_theme_mod('pet_care_clinic_social_gp',''));?>"><i class="fab fa-google-plus-g"></i></a>
                                </span>
                            <?php endif; ?>
                            <?php if(get_theme_mod("pet_care_clinic_social_linkedin")) : ?>
                                <span class="lis">
                                    <a href="<?php echo esc_url(get_theme_mod('pet_care_clinic_social_linkedin',''));?>"><i class="fab fa-linkedin"></i></a>
                                </span>
                            <?php endif; ?>
                            <?php if(get_theme_mod("pet_care_clinic_social_skype")) : ?>
                                <span class="sks">
                                    <a href="skype:skype?<?php echo esc_url(get_theme_mod('pet_care_clinic_social_skype',''));?>" title="" data-placement="bottom" data-toggle="tooltip" data-original-title="Skype"><i class="fab fa-skype"></i></a>
                                </span>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    </div>