=== Pet Care Clinic Responsive Wordpress Theme ===
Contributors: UnboxThemes
Tags: one-column, two-columns, right-sidebar, left-sidebar, custom-background, custom-menu, featured-images, footer-widgets, sticky-post, threaded-comments, translation-ready, blog, e-commerce, portfolio
Requires at least: 4.3
Tested up to: 5.0.3
Stable tag: 2.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Pet Care Clinic is a charming, elegant, versatile, technologically advanced and visually pleasing pet WordPress theme to be used by animal selling shops, pet stores, pet grooming parlours, dog cleaning centre, animal food supplier, dog and pet service provider, dog washing centre, pet health consultant, animal care centre and other such businesses. You can also use it as an animal and pet blog to give tips on pet care.
 
== Description ==

Pet Care Clinic is a charming, elegant, versatile, technologically advanced and visually pleasing pet WordPress theme to be used by animal selling shops, pet stores, pet grooming parlours, dog cleaning centre, animal food supplier, dog and pet service provider, dog washing centre, pet health consultant, animal care centre and other such businesses. You can also use it as an animal and pet blog to give tips on pet care. This animal WordPress theme is filled with eye-catching colours and beautiful fonts to make it more appealing. It is a fully responsive theme that can be accessed on any device and screen resolution. It is multi-browser compatible, multilingual and supports RTL writing. The layout of this theme can be changed from full screen to full-width and boxed version. This pet WordPress theme is compatible with all the third party plugins and integrated with WooCommerce so you can set up your online store in just a few clicks. It is embedded with social media icons to easily promote your services and the SEO of this theme will reserve a higher rank for your website in search engine results. Its developers have designed several purpose oriented sections to serve diverse needs of this field. Get this user-friendly and professional looking theme for your pet business now.

== Changelog ==

Version 1.0
	i) Intial version Release

Version 2.0
	i) Done the changes in search form.
	ii) Resolved some theme check error.
	iii) Done customization in css.
	iv) Changed images in screenshot and added new link in raedme file.

Version 2.1
	i) Added the pingback function in the function.php file.
	ii) Removed the "pet_care_clinic_content()" from function.php.
	iii) Removed the "pet_care_clinic_postview()" function.

Version 2.2
	i) Added minified js files in js folder.
	ii) Prefixed in sidebar do_action.
	iii) Added animate.css license
	iv) Added author url in stylesheet.
	v)  Done prefixing for custom js in function file.
	vi) Removed the unwanted classes.

Version 2.3
	i) Added minified js files and non minified js files.
	ii) changed the name of ccustom script js file.
	
== Resources ==

Pet Care Clinic WordPress Theme, Copyright 2018 UnboxThemes
Pet Care Clinic is distributed under the terms of the GNU GPL.

1 - Bootstrap (http://getbootstrap.com/) licensed under MIT license (http://opensource.org/licenses/mit-license.html)

2 - FontAwesome (http://fontawesome.io) licensed under the SIL OFL 1.1 (http://scripts.sil.org/OFL), CSS: MIT License(http://opensource.org/licenses/mit-license.html)

3 - WOW.js by Matthieu Aussaguel; Licensed MIT (http://opensource.org/licenses/mit-license.html)

4 - Query meanMenu by MeanThemes licensed under the GPLv2 license (http://www.gnu.org/licenses/gpl-2.0.html)

5 - carouFredSel by caroufredsel.dev7studios.com Dual licensed under the MIT and GPL licenses. (http://en.wikipedia.org/wiki/GNU_General_Public_License)

6 - jQuery Nivo Slider Licensed MIT (http://opensource.org/licenses/mit-license.html)

7 - jQuery-ui by jQuery Foundation projects licensed specified if not Licensed MIT (http://opensource.org/licenses/mit-license.html)

8 - Fancybox Licensed under both MIT and GPL licenses (http://www.gnu.org/licenses/gpl-2.0.html)(http://opensource.org/licenses/mit-license.html)

9 - Montserrat,Raleway,Mr Bedford,PT Sans and GreatVibes fonts which all are licensed under the SIL Open Font License(http://scripts.sil.org/OFL)

10 - WP Bootstrap Navwalker A custom WordPress nav walker class to implement the Bootstrap 3 navigation. (https://github.com/twittem/wp-bootstrap-navwalker) licensed under GPL-2.0+ license (http://www.gnu.org/licenses/gpl-2.0.txt)

11 - all images and icons used with jQuery-ui are by jQuery-ui licensed specified if not Licensed MIT (http://opensource.org/licenses/mit-license.html)

12 - Animate.css is licensed under the MIT license. (http://opensource.org/licenses/MIT)

13 - Stocksnap Images, 
License: CC0 1.0 Universal (CC0 1.0)
Source: https://stocksnap.io/license

Header Image, Copyright Lennart kcotsttiw
License: CC0 1.0 Universal (CC0 1.0)
Source: https://stocksnap.io/photo/N52DFYMO8P

Services Image, Copyright Andrew Branch
License: CC0 1.0 Universal (CC0 1.0)
Source: https://stocksnap.io/photo/MAVAL070JE

Services Image, Copyright 	Pozan Matang
License: CC0 1.0 Universal (CC0 1.0)
Source: https://stocksnap.io/photo/7GEZBBYE4Q